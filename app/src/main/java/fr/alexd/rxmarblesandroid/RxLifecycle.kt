package fr.alexd.rxmarblesandroid

import android.arch.lifecycle.GenericLifecycleObserver
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

data class SubscriptionScope(
        val start: Lifecycle.Event = Lifecycle.Event.ON_RESUME,
        val stop: Lifecycle.Event = Lifecycle.Event.ON_PAUSE,
        val keepAliveStart: Lifecycle.Event? = null,
        val keepAliveStop: Lifecycle.Event? = null) {

    companion object {
        val whenResumed = SubscriptionScope()

        val whenResumedKeepAliveWhileCreated = SubscriptionScope(
                keepAliveStart = Lifecycle.Event.ON_CREATE,
                keepAliveStop = Lifecycle.Event.ON_DESTROY
        )
    }
}

fun <T> Observable<T>.subscribeWithLifecycle(lifecycleOwner: LifecycleOwner,
                                             scope: SubscriptionScope = SubscriptionScope.whenResumedKeepAliveWhileCreated,
                                             onNext: (T) -> Unit) {
    _subscribeWithLifecycle(lifecycleOwner, scope) { subscribe(onNext) }
}

fun <T> Observable<T>.subscribeWithLifecycle(lifecycleOwner: LifecycleOwner,
                                             scope: SubscriptionScope = SubscriptionScope.whenResumedKeepAliveWhileCreated,
                                             onNext: (T) -> Unit, onError: (Throwable) -> Unit) {
    _subscribeWithLifecycle(lifecycleOwner, scope) { subscribe(onNext, onError) }
}

fun <T> Observable<T>.subscribeWithLifecycle(lifecycleOwner: LifecycleOwner,
                                             scope: SubscriptionScope = SubscriptionScope.whenResumedKeepAliveWhileCreated,
                                             onNext: (T) -> Unit, onError: (Throwable) -> Unit, onComplete: () -> Unit) {
    _subscribeWithLifecycle(lifecycleOwner, scope) { subscribe(onNext, onError, onComplete) }
}

private fun <T> Observable<T>._subscribeWithLifecycle(lifecycleOwner: LifecycleOwner,
                                                      scope: SubscriptionScope,
                                                      subscribeFn: Observable<T>.() -> Disposable) {
    var keepAliveSubscription: Disposable? = null
    var subscription: Disposable? = null

    lifecycleOwner.lifecycle.addObserver(GenericLifecycleObserver { _, event ->
        when (event) {
            scope.keepAliveStart -> keepAliveSubscription = subscribe()
            scope.start -> subscription = subscribeFn()
            scope.stop -> subscription?.dispose()?.let { subscription = null }
            scope.keepAliveStop -> keepAliveSubscription?.dispose()?.let { keepAliveSubscription = null }
            else -> {}
        }
    })
}