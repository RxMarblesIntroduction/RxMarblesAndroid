package fr.alexd.rxmarblesandroid

import android.app.Application
import com.github.salomonbrys.kodein.*
import fr.alexd.rxmarblesandroid.services.WeatherService
import fr.alexd.rxmarblesandroid.services.github.GithubApi
import fr.alexd.rxmarblesandroid.services.github.GithubService
import fr.alexd.rxmarblesandroid.services.openweathermap.OpenWeatherMapApi
import fr.alexd.rxmarblesandroid.services.openweathermap.OpenWeatherMapService
import fr.alexd.rxmarblesandroid.ui.weather.WeatherViewModel
import fr.alexd.rxmarblesandroid.ui.weather.WeatherViewModelImpl
import fr.alexd.rxmarblesandroid.ui.github.GithubSearchViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by Alexandre Delattre on 07/09/2017.
 */
open class WeatherApplication : Application(), KodeinAware {
    override val kodein by Kodein.lazy {
        bind<Retrofit>("openweathermap") with singleton {
            buildRetrofit("http://api.openweathermap.org/data/2.5/")
        }

        bind<OpenWeatherMapApi>() with singleton {
            instance<Retrofit>("openweathermap").create(OpenWeatherMapApi::class.java)
        }

        bind<Retrofit>("github") with singleton {
            buildRetrofit("https://api.github.com/")
        }

        bind<GithubApi>() with singleton {
            instance<Retrofit>("github").create(GithubApi::class.java)
        }

        bind<WeatherService>() with singleton {
            OpenWeatherMapService(instance(), "a936c94519e94feff4f84f095dc3fe76")
        }

        bind<GithubService>() with singleton {
            GithubService(instance())
        }

        bind<WeatherViewModel>() with provider {
            WeatherViewModelImpl(instance(), AndroidSchedulers.mainThread())
        }

        bind<GithubSearchViewModel>() with provider {
            GithubSearchViewModel(instance(), AndroidSchedulers.mainThread())
        }

    }
}

fun buildRetrofit(baseURL: String): Retrofit {
    return Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .build()
}