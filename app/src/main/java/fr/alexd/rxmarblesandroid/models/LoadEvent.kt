package fr.alexd.rxmarblesandroid.models

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * Created by Alexandre Delattre on 12/11/2017.
 */
sealed class LoadEvent<out T> {
    object Loading : LoadEvent<Nothing>()
    data class Loaded<out T>(val data: T): LoadEvent<T>()
    data class Error(val e: Throwable) : LoadEvent<Nothing>()
}

fun <T> Single<T>.toLoadEvent(scheduler: Scheduler): Observable<LoadEvent<T>> = toObservable()
        .map<LoadEvent<T>> { LoadEvent.Loaded(it) }
        .onErrorReturn { LoadEvent.Error(it) }
        .observeOn(scheduler)
        .startWith(LoadEvent.Loading)