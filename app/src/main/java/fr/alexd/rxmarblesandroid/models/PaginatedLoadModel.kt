package fr.alexd.rxmarblesandroid.models

import io.reactivex.Observable
import io.reactivex.Observable.just
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * Created by Alexandre Delattre on 04/11/2017.
 */
sealed class PaginatedLoadModel<out T> {
    abstract val current: List<T>

    data class Loaded<out T>(override val current: List<T>) : PaginatedLoadModel<T>()
    data class Loading<out T>(override val current: List<T>) : PaginatedLoadModel<T>()
    data class Error<out T>(override val current: List<T>, val error: Throwable) : PaginatedLoadModel<T>()
}

typealias LoadFunction<Key, Item> = (Key) -> Single<Pair<List<Item>, Key?>>

fun <T> PaginatedLoadModel<T>.step(event: LoadEvent<List<T>>): PaginatedLoadModel<T> = when (event) {
    LoadEvent.Loading -> PaginatedLoadModel.Loading(current)
    is LoadEvent.Loaded -> PaginatedLoadModel.Loaded(current + event.data)
    is LoadEvent.Error -> PaginatedLoadModel.Error(current, event.e)
}

fun <Key, Item> paginatedLoad(
    input: Observable<Unit>, initialKey: Key,
    mainScheduler: Scheduler,
    loadFunction: LoadFunction<Key, Item>
): Observable<PaginatedLoadModel<Item>> {
    fun loadPage(key: Key, firstPage: Boolean = false): Observable<LoadEvent<List<Item>>> =
        (if (firstPage) just(Unit) else input.take(1)).concatMap {
            loadFunction(key).toLoadEvent(mainScheduler)
        }.concatMap {
            when (it) {
                is LoadEvent.Loading -> just(it)
                is LoadEvent.Error -> just<LoadEvent<List<Item>>>(it).concatWith(loadPage(key))
                is LoadEvent.Loaded -> {
                    val (data, nextKey) = it.data
                    val loadedEvent = just<LoadEvent<List<Item>>>(LoadEvent.Loaded(data))
                    nextKey?.let { loadedEvent.concatWith(loadPage(it)) } ?: loadedEvent
                }
            }
        }

    val first: PaginatedLoadModel<Item> = PaginatedLoadModel.Loaded(emptyList())
    return loadPage(initialKey, firstPage = true).scan(first) { acc, event -> acc.step(event) }.skip(1)
}