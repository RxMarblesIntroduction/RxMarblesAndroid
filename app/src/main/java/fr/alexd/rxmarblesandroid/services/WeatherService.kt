package fr.alexd.rxmarblesandroid.services

import fr.alexd.rxmarblesandroid.models.WeatherData
import io.reactivex.Single

interface WeatherService {

    fun getWeather(city: String): Single<WeatherData>
}


