package fr.alexd.rxmarblesandroid.services.github

import com.squareup.moshi.Json
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Alexandre Delattre on 05/11/2017.
 */

data class Repository(val id: String, val name: String, @Json(name = "stargazers_count") val stars: Int)

data class SearchRepositoryResponse(val items: List<Repository>)

interface GithubApi {
    @GET("/search/repositories")
    fun searchRepositories(@Query("q") search: String,
                           @Query("page") page: Int,
                           @Query("sort") sort: String,
                           @Query("order") order: String,
                           @Query("per_page") perPage: Int): Single<Response<SearchRepositoryResponse>>
}