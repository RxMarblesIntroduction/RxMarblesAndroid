package fr.alexd.rxmarblesandroid.services.github

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Query

/**
 * Created by Alexandre Delattre on 05/11/2017.
 */
class GithubService(val api: GithubApi) {
    fun searchRepositories(@Query("q") search: String,
                           @Query("page") page: Int,
                           @Query("sort") sort: String = "stars",
                           @Query("order") order: String = "desc",
                           @Query("per_page") perPage: Int = 30): Single<Pair<List<Repository>, Int?>> =
            api.searchRepositories(search, page, sort, order, perPage).map { resp ->
                if (resp.isSuccessful) {
                    (resp.body()?.items ?: error("Empty body")) to (page + 1).takeIf { hasNextHeader(resp) }
                } else throw GithubApiException(resp.code())
            }
}

private fun hasNextHeader(response: Response<*>) = response.headers()["Link"]?.contains("rel=\"next\"") ?: false

class GithubApiException(val httpCode: Int): Exception()