package fr.alexd.rxmarblesandroid.services.openweathermap

import fr.alexd.rxmarblesandroid.models.WeatherData
import fr.alexd.rxmarblesandroid.services.WeatherService
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class OpenWeatherMapService(val api: OpenWeatherMapApi, val appId: String) : WeatherService {
    override fun getWeather(city: String) = api.getWeather(appId, city).map { (main, name, weather) ->
        WeatherData(name,
                urlForOpenWeatherMapPicto(weather.first().icon),
                main.temp_min,
                main.temp_max)
    }.delay(1, TimeUnit.SECONDS)
}


fun urlForOpenWeatherMapPicto(picto: String) = "http://openweathermap.org/img/w/$picto.png"