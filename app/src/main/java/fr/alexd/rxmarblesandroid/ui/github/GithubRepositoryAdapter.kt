package fr.alexd.rxmarblesandroid.ui.github

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.alexd.rxmarblesandroid.R
import fr.alexd.rxmarblesandroid.models.PaginatedLoadModel
import fr.alexd.rxmarblesandroid.services.github.Repository
import kotlinx.android.synthetic.main.item_github_error.view.retryButton
import kotlinx.android.synthetic.main.item_github_repo.view.*

class GithubRepositoryAdapter(private val repositories: PaginatedLoadModel<Repository>,
                              private val retryAction: () -> Unit)
    : RecyclerView.Adapter<GithubAdapterViewHolder>() {

    override fun getItemCount() = repositories.current.size + when (repositories) {
        is PaginatedLoadModel.Loaded -> 0
        else -> 1
    }

    override fun getItemViewType(position: Int) =
            if (position < repositories.current.size) 0 else when (repositories) {
                is PaginatedLoadModel.Loading -> 1
                is PaginatedLoadModel.Error -> 2
                else -> error("Unreachable")
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LayoutInflater.from(parent.context).let {
        when (viewType) {
            0 -> RepositoryViewHolder(it.inflate(R.layout.item_github_repo, parent, false))
            1 -> LoadingViewHolder(it.inflate(R.layout.item_github_loading, parent, false))
            2 -> ErrorViewHolder(it.inflate(R.layout.item_github_error, parent, false), retryAction)
            else -> error("Unreachable")
        }
    }

    override fun onBindViewHolder(holder: GithubAdapterViewHolder, position: Int) {
        when (holder) {
            is RepositoryViewHolder -> holder.displayRepository(repositories.current[position])
        }

    }
}

sealed class GithubAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

class RepositoryViewHolder(itemView: View) : GithubAdapterViewHolder(itemView) {
    fun displayRepository(repository: Repository) {
        itemView.textViewTitle.text = "${repository.name} (${repository.stars} ✩)"
    }
}

class LoadingViewHolder(itemView: View) : GithubAdapterViewHolder(itemView)

class ErrorViewHolder(itemView: View, private val retryAction: () -> Unit) : GithubAdapterViewHolder(itemView) {
    init {
        itemView.retryButton.setOnClickListener { retryAction() }
    }
}