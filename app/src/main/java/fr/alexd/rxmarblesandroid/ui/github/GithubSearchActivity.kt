package fr.alexd.rxmarblesandroid.ui.github

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.android.KodeinSupportFragment
import com.github.salomonbrys.kodein.instance
import fr.alexd.rxmarblesandroid.R
import fr.alexd.rxmarblesandroid.dismissKeyboard
import fr.alexd.rxmarblesandroid.subscribeWithLifecycle
import kotlinx.android.synthetic.main.activity_github_search.*


class GithubSearchActivity : KodeinAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(android.R.id.content, GithubSearchFragment())
                    .commit()
        }
    }
}

class GithubSearchFragment : KodeinSupportFragment() {
    val vm: GithubSearchViewModel by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        vm.results.subscribeWithLifecycle(this) {
            recyclerView.swapAdapter(GithubRepositoryAdapter(it, { vm.loadMore.onNext(Unit) }), false)
        }

        vm.loadOnScrollEnabled.subscribeWithLifecycle(this) {
            recyclerView.removeOnScrollListener(onScrollListener)
            if (it) recyclerView.addOnScrollListener(onScrollListener)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.activity_github_search, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        searchView.apply {
            setIconifiedByDefault(false)
            setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    dismissKeyboard()
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let { vm.search.onNext(it) }
                    return true
                }
            })
        }

        recyclerView.layoutManager = LinearLayoutManager(context)
    }

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            if (dy > 0) {
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                    vm.loadMore.onNext(Unit)
                }
                searchView.dismissKeyboard()
            }
        }
    }
}