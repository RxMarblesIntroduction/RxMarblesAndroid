package fr.alexd.rxmarblesandroid.ui.github

import fr.alexd.rxmarblesandroid.models.PaginatedLoadModel
import fr.alexd.rxmarblesandroid.models.paginatedLoad
import fr.alexd.rxmarblesandroid.services.github.GithubService
import fr.alexd.rxmarblesandroid.services.github.Repository
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit

class GithubSearchViewModel(api: GithubService, mainScheduler: Scheduler) {
    val search: Subject<String> = PublishSubject.create<String>()
    val loadMore: Subject<Unit> = PublishSubject.create<Unit>()
    val results = githubSearchViewModel(search, loadMore, api, mainScheduler).replay(1).refCount()
    val loadOnScrollEnabled = results.map { it !is PaginatedLoadModel.Error }.distinctUntilChanged()
}

fun githubSearchViewModel(search: Observable<String>, loadMore: Observable<Unit>,
                          api: GithubService, mainScheduler: Scheduler): Observable<PaginatedLoadModel<Repository>> =
        search.filter { it.isNotBlank() }
                .distinctUntilChanged()
                .debounce(500, TimeUnit.MILLISECONDS, mainScheduler)
                .switchMap { s ->
                    paginatedLoad(loadMore, 1, mainScheduler) { p -> api.searchRepositories(s, p) }
                }