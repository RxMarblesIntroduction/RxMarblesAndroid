package fr.alexd.rxmarblesandroid.ui.weather

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import com.squareup.picasso.Picasso
import fr.alexd.rxmarblesandroid.R
import fr.alexd.rxmarblesandroid.addSimpleTextChangedListener
import fr.alexd.rxmarblesandroid.disposedBy
import fr.alexd.rxmarblesandroid.models.LoadEvent
import fr.alexd.rxmarblesandroid.models.WeatherData
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

class WeatherActivity : KodeinAppCompatActivity() {

    val viewModel: WeatherViewModel by instance()

    val disposeBag = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initBindings()
    }

    fun initBindings() {
        searchEditText.addSimpleTextChangedListener {
            viewModel.city.onNext(it)
        }

        viewModel.weather.subscribe {
            when (it) {
                LoadEvent.Loading -> {
                    displayLoader(true)
                    displayWeatherLayout(false)
                }
                is LoadEvent.Loaded -> {
                    displayLoader(false)
                    displayWeatherLayout(true)
                    displayWeather(it.data)
                }
                is LoadEvent.Error -> {
                    displayLoader(false)
                    displayError(it.e)
                }
            }
        }.disposedBy(disposeBag)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }


    private fun displayLoader(loading: Boolean) {
        progressBar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    private fun displayWeatherLayout(visible: Boolean) {
        weatherLayout.visibility = if (visible) View.VISIBLE else View.GONE
    }

    private fun displayWeather(data: WeatherData) {
        Picasso.with(this).load(data.pictoUrl).into(imageView)
        textViewMinTemp.text = "${data.minTemperature} °C"
        textViewMaxTemp.text = "${data.maxTemperature} °C"
    }

    private fun displayError(e: Throwable) {
        Snackbar.make(searchEditText, "Erreur : ${e.localizedMessage}", Snackbar.LENGTH_SHORT).show()
    }
}