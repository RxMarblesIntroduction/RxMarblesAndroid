package fr.alexd.rxmarblesandroid.ui.weather

import fr.alexd.rxmarblesandroid.models.LoadEvent
import fr.alexd.rxmarblesandroid.models.WeatherData
import io.reactivex.Observable
import io.reactivex.subjects.Subject

/**
 * Created by Alexandre Delattre on 07/09/2017.
 */
interface WeatherViewModel {
    // Inputs
    val city: Subject<String>
    // Outputs
    val weather: Observable<LoadEvent<WeatherData>>
}