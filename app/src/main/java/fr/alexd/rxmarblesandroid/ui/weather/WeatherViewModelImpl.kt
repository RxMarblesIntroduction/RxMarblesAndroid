package fr.alexd.rxmarblesandroid.ui.weather

import fr.alexd.rxmarblesandroid.models.LoadEvent
import fr.alexd.rxmarblesandroid.models.WeatherData
import fr.alexd.rxmarblesandroid.models.toLoadEvent
import fr.alexd.rxmarblesandroid.services.WeatherService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit


class WeatherViewModelImpl(weatherService: WeatherService,
                           mainScheduler: Scheduler) : WeatherViewModel {

    override val city: Subject<String>
    override val weather: Observable<LoadEvent<WeatherData>>

    init {
        city = BehaviorSubject.createDefault("")
        weather = weatherViewModel(weatherService, mainScheduler, city)
    }
}

fun weatherViewModel(weatherService: WeatherService,
                     mainScheduler: Scheduler,
                     city: Observable<String>): Observable<LoadEvent<WeatherData>> = city
        .filter { it.isNotEmpty() }
        .debounce(500, TimeUnit.MILLISECONDS, mainScheduler)
        .switchMap { weatherService.getWeather(it).toLoadEvent(mainScheduler) }

