package fr.alexd.rxmarblesandroid

import fr.alexd.rxmarblesandroid.models.PaginatedLoadModel
import fr.alexd.rxmarblesandroid.models.paginatedLoad
import io.reactivex.marble.MarbleScheduler
import org.junit.Before
import org.junit.Test

/**
 * Created by Alexandre Delattre on 08/09/2017.
 */
class PaginatedLoadTest {
    lateinit var scheduler: MarbleScheduler

    val pages = mapOf(
            "1" to (listOf(1, 2, 3) to 2),
            "2" to (listOf(4, 5, 6) to null)
    )
    val pageError = IllegalStateException("Cannot retrieve page")
    val resultValues = mapOf(
            "a" to PaginatedLoadModel.Loading(listOf()),
            "1" to PaginatedLoadModel.Loaded(listOf(1, 2, 3)),
            "b" to PaginatedLoadModel.Loading(listOf(1, 2, 3)),
            "2" to PaginatedLoadModel.Loaded(listOf(1, 2, 3, 4, 5, 6)),
            "e" to PaginatedLoadModel.Error(listOf(), pageError)
    )

    @Before
    fun setup() {
        scheduler = MarbleScheduler(100)
    }

    @Test
    fun testSimple() {
        val s = scheduler
        val input = s.hot(    "c-----c------", mapOf("c" to Unit))

        val result = paginatedLoad(input, 1, scheduler) { page ->
            when (page) {
                1 -> s.single("--1", pages)
                2 -> s.single("--2", pages)
                else -> error("Should not be called")
            }
        }

        s.expectObservable(result)
                .toBe(       "a-1---b-(2|)-", resultValues)
        s.flush()
    }

    @Test
    fun testFilteredClick() {
        val s = scheduler
        val input = s.hot(    "ccc---ccc-----ccc", mapOf("c" to Unit))

        val result = paginatedLoad(input, 1, scheduler) { page ->
            when (page) {
                1 -> s.single("--1", pages)
                2 -> s.single("--2", pages)
                else -> error("Should not be called")
            }
        }

        s.expectObservable(result)
                .toBe(       "a-1---b-(2|)-", resultValues)
        s.flush()
    }

    @Test
    fun testError() {
        val s = scheduler
        val input = s.hot(       "ccc---ccc---ccc", mapOf("c" to Unit))

        var callCount = 0
        val result = paginatedLoad(input, 1, scheduler) { page ->
            when (page) {
                1 ->
                    if (callCount == 0) {
                        callCount += 1
                        s.single("--1", pages).map<Pair<List<Int>, Int?>> { throw pageError }
                    } else {
                        s.single("--1", pages)
                    }
                2 -> s.single("--2", pages)
                else -> error("Should not be called")
            }
        }

        s.expectObservable(result)
                .toBe(         "a-e---a-1---b-(2|)", resultValues)
        s.flush()
    }

}

