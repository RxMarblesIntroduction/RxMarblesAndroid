package fr.alexd.rxmarblesandroid

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.marble.MarbleScheduler

/**
 * Created by Alexandre Delattre on 04/11/2017.
 */
fun <T> MarbleScheduler.hot(marbles: String, values: Map<String, T>): Observable<T> = createHotObservable(marbles, values)
fun <T> MarbleScheduler.single(marbles: String, values: Map<String, T>): Single<T> =
        createColdObservable<T>(marbles, values).take(1).singleOrError()