package fr.alexd.rxmarblesandroid

import fr.alexd.rxmarblesandroid.models.LoadEvent
import fr.alexd.rxmarblesandroid.models.WeatherData
import fr.alexd.rxmarblesandroid.services.WeatherService
import fr.alexd.rxmarblesandroid.ui.weather.weatherViewModel
import io.reactivex.marble.MarbleScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Created by Alexandre Delattre on 08/09/2017.
 */
class WeatherViewModelTestWithMarbles {
    lateinit var weatherService: WeatherService
    lateinit var scheduler: MarbleScheduler

    val weatherData = WeatherData("toulouse", "sunny", 20f, 30f)
    val bordeauxData = WeatherData("bordeaux", "cloudy", 10f, 15f)
    val weatherError = Exception()

    val cityValues = mapOf(
        "0" to "",
            "1" to "tou",
            "t" to "toulouse",
            "b" to "bordeaux"
    )

    val stateValues = mapOf(
            "l" to LoadEvent.Loading,
            "e" to LoadEvent.Error(weatherError),
            "t" to LoadEvent.Loaded(weatherData),
            "b" to LoadEvent.Loaded(bordeauxData)
    )

    val weatherValues = mapOf(
            "t" to weatherData,
            "b" to bordeauxData
    )

    @Before
    fun setup() {
        weatherService = Mockito.mock(WeatherService::class.java)
        scheduler = MarbleScheduler(100)
    }

    @Test
    fun testSimple() {
        val s = scheduler
        val cityInput = s.hot(                      "-----t-------", cityValues)
        `when`(weatherService.getWeather("toulouse"))
                // debouncing                             -----t
                .thenReturn(s.single(                         "--t", weatherValues))

        val weather = weatherViewModel(weatherService, s, cityInput)
        s.expectObservable(weather).toBe(           "----------l-t", stateValues)
        s.flush()
    }

    @Test
    fun test2Cities() {
        val s = scheduler
        val cityInput = s.hot(                      "0-1-t------------b----------", cityValues)
        `when`(weatherService.getWeather("toulouse"))
                // debouncing                            -----t       -----b
                .thenReturn(s.single(                        "--t", weatherValues))
        `when`(weatherService.getWeather("bordeaux"))
                .thenReturn(s.single(                                     "--b", weatherValues))

        val weather = weatherViewModel(weatherService, s, cityInput)
        s.expectObservable(weather).toBe(           "---------l-t----------l-b---", stateValues)
        s.flush()
    }

    @Test
    fun test2CitiesInterleaved() {
        val s = scheduler
        val cityInput = s.hot(                      "0-1-t-------b----------", cityValues)
        `when`(weatherService.getWeather("toulouse"))
                // debouncing                            -----t  -----b
                .thenReturn(s.single(                        "-----------t", weatherValues))
        `when`(weatherService.getWeather("bordeaux"))
                .thenReturn(s.single(                                "--b", weatherValues))

        val weather = weatherViewModel(weatherService, s, cityInput)

        s.expectObservable(weather).toBe(           "---------l-------l-b---", stateValues)
        s.flush()
    }
}